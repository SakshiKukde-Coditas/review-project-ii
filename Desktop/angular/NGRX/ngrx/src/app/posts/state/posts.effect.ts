import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, of, switchMap } from 'rxjs';
import { PostsService } from 'src/app/services/posts.service';
import { addPost, addPostSuccess, deletePost, deletePostSuccess, loadPosts, loadPostsSuccess, updatePost, updatePostSuccess } from './posts.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app.state';
import { Injectable, Type, forwardRef } from '@angular/core';

@Injectable()

export class PostsEffects {
  constructor(
    private actions$: Actions,
    private postsService: PostsService,
    private store: Store<AppState>
  ) 
  {}

  loadPosts$ = createEffect(() => {
    console.log('inside load posts');
    return this.actions$.pipe(
      ofType(loadPosts),
      mergeMap((action) => {
        return this.postsService.getPosts().pipe(
          map((posts) => {
            console.log(posts);
            const newKey = 'srno';
            for (let i = 0; i < posts.length; i++) {
            posts[i][newKey] = (i + 1).toString();
            }
            console.log(posts);
            return loadPostsSuccess({ posts });
          })
        );
      })
    );
  });

  addPosts$ = createEffect(() => {
    return this.actions$.pipe(
        ofType(addPost), 
        mergeMap((action) => {
        return this.postsService.addPosts(action.post).pipe(
            map(data => {
            console.log(data);
            const post = {...action.post, id: action.post.id};
            return addPostSuccess({ post });
        })
        );
    })
    );
  } );

  updatePosts$ = createEffect(() => {
    return this.actions$.pipe(
        ofType(updatePost), 
        switchMap((action) => {
        return this.postsService.updatePosts(action.post).pipe(
            map(data => { 
            console.log(data);
            // const post = {...action.post, id: action.post.id};
            return updatePostSuccess({ post: action.post });
        })
        );
    })
    );
  } );

  deletePosts$ = createEffect(() => {
    return this.actions$.pipe(
        ofType(deletePost), 
        switchMap((action:any) => {
        return this.postsService.deletePosts(action.id).pipe(
            map(data => { 
            console.log(data);
            // const post = {...action.post, id: action.post.id};
            return deletePostSuccess({ id: action.id });
        })
        );
    })
    );
  } );
}
