import { createFeatureSelector, createSelector } from "@ngrx/store";
import { PostsState } from "./posts.state";

export const POST_STATE_NAME = 'posts';
const getPostsState = createFeatureSelector<PostsState>(POST_STATE_NAME);

export const getPosts = createSelector(getPostsState, (state) => {
  console.log(state.posts);
    return state.posts;
})

export const getPostById = (props: {id: any}) =>   
  createSelector(getPostsState,
    (state) => state.posts.find((post) => post.id === props.id));

