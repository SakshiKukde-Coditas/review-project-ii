import { Posts } from "src/app/models/posts.model";

export interface PostsState {
    posts: Posts[];
}

export const initialState: PostsState = {
    posts: []
}