import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app.state';
import { select } from '@ngrx/store';
import * as fromRoot from '../state/posts.selector';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { updatePost } from '../state/posts.actions';



@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit, OnDestroy{

  post: any = {};
  postForm!:FormGroup;
  postSubcription!: Subscription;
  constructor(private route: ActivatedRoute, private store: Store<AppState>, private router: Router){
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params)=>{
      const id = params.get('id');
      console.log(id);
      this.postSubcription = this.store.pipe(
        select(fromRoot.getPostById({ id }))
      ).subscribe(data => {
        this.post = data;
        console.log(this.post);
        this.createForm();
      })
    })
   
  }
  onUpdatePost() {
    if(!this.postForm) {
      return;
    }
    const name = this.postForm.value.name;
    const email = this.postForm.value.email;
    const number = this.postForm.value.number;

    const post:any = {
      id: this.post.id,
      name,
      email,
      number 
    };
    this.store.dispatch(updatePost({post}));
    this.router.navigate(['posts']);
  }

  createForm() {
    this.postForm = new FormGroup({
      name: new FormControl(this.post.name, [Validators.required,Validators.minLength(6)]),
      email: new FormControl(this.post.email,[Validators.required, Validators.email,Validators.minLength(6)]),
      number: new FormControl(this.post.number, [Validators.required, Validators.maxLength(10)])
    });
  }
  ngOnDestroy(): void {
    if(this.postSubcription) {
      this.postSubcription.unsubscribe();
    }
  }
 
}
