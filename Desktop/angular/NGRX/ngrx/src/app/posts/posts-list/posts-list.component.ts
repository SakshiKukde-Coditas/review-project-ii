import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Posts } from 'src/app/models/posts.model';
import { AppState } from 'src/app/state/app.state';
import { getPosts } from '../state/posts.selector';
import { deletePost, loadPosts } from '../state/posts.actions';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit{

  posts!: Observable<Posts[]>;

  constructor(private store: Store<AppState>){}

  ngOnInit(): void {
    this.posts = this.store.select(getPosts);
    this.store.dispatch(loadPosts());
    console.log(this.posts);
    
  }

  onDeletePost(id:any) {
    console.log(id);
    if(confirm('Are you sure you want tot delet')) {
      console.log(id);
      this.store.dispatch(deletePost({id}));
    }
    return;
  }

}
