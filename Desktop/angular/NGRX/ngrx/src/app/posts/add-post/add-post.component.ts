import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { Posts } from 'src/app/models/posts.model';
import { AppState } from 'src/app/state/app.state';
import { addPost } from '../state/posts.actions';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit{

  postForm!: FormGroup;
  constructor(private store: Store<AppState>){}

  ngOnInit(): void {
    this.postForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      email: new FormControl(null, [Validators.required, Validators.email, Validators.minLength(6)]),
      number: new FormControl(null, [Validators.required, Validators.minLength(10)])
    })
  }

  onAddPost() {
    console.log("inside add post", this.postForm);
    // if(this.postForm.valid) {
    //   return;
    // }
    const post: Posts =  {
      name: this.postForm.value.name,
      email: this.postForm.value.email,
      number: this.postForm.value.number,
    };
    this.store.dispatch(addPost({post}));
  }
}
