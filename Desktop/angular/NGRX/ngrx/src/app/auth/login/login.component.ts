import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { loginStart } from '../state/auth.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app.state';
import { setLoadingSpinner } from 'src/app/state/shared/shared.action';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  loginForm!: FormGroup;
  constructor(private store: Store<AppState>){}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required,])
    })
  }

  onLogin() {
    console.log("inside login", this.loginForm.value);
    const username = this.loginForm.value.username;
    const password = this.loginForm.value.password;
    console.log(username, password);
    this.store.dispatch(setLoadingSpinner({status: true}));
    this.store.dispatch(loginStart({username, password}));

  }
}
