import { RouterModule, Routes } from "@angular/router";
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { LoginComponent } from './login/login.component';
import { AUTH_STATE_NAME } from "./state/auth.selectors";
import { AuthReducer } from "./state/auth.reducer";
import { StoreModule } from "@ngrx/store";
import { AuthEffects } from "./state/auth.effects";
import { SignupComponent } from './signup/signup.component';
import { EffectsModule } from "@ngrx/effects";
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: '',
        children: [
            // {path:'', redirectTo: 'login'},
            {path:'login', component: LoginComponent},
            {path:'signup', component: SignupComponent}
        ]
      },
];

@NgModule({
    declarations: [
    LoginComponent,
    SignupComponent
  ],
   imports: [CommonModule, ReactiveFormsModule,
    EffectsModule.forFeature([AuthEffects]),
    // StoreModule.forFeature(AUTH_STATE_NAME, AuthReducer),
    RouterModule.forChild(routes)]
})

export class AuthModule {

}