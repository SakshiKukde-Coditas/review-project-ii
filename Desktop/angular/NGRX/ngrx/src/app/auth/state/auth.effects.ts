import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, exhaustMap, map, of, tap } from "rxjs";
import { AuthService } from "src/app/services/auth.service";
import { loginStart, loginSuccess, signupStart, signupSuccess } from "./auth.actions";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/state/app.state";
import { setErrorMessage, setLoadingSpinner } from "src/app/state/shared/shared.action";
import { Router } from "@angular/router";

@Injectable()

export class AuthEffects {

    constructor(private actions$: Actions, private authService: AuthService, private store: Store<AppState>, private router: Router){}

    login$ = createEffect(()=> {
        return this.actions$.pipe(ofType(loginStart), exhaustMap((action:any) => {
            console.log(action);
            return this.authService.login(action.username, action.password).pipe(map((data:any) => {
                this.store.dispatch(setLoadingSpinner({status: false}));
                this.store.dispatch(setErrorMessage({message: ''}));
                const user = this.authService.formatUser(data);
                console.log(user);
                // this.authService.setUserInLocalStorage(user);
                return loginSuccess({user});
            }),
            catchError(error => {
                // console.log(error.error.error.message);
                this.store.dispatch(setLoadingSpinner({status: false}));
                const errorMessage = this.authService.getErrorMessage(error.error.error.message);
                return of(setErrorMessage({ message: errorMessage}));
            })
            );
        }))
    });


    loginRedirect$ = createEffect(() => {
        return this.actions$.pipe(ofType(loginSuccess), tap(action => {
            this.store.dispatch(setErrorMessage({ message: ''}));
            this.router.navigate(['/'])
        })
        )},{dispatch: false}
    );


    //dont have signUp api so kept as commented

    // signUp$ = createEffect(()=> {
    //     return this.actions$.pipe(ofType(signupStart), exhaustMap((action) => {
    //         console.log(action);
    //         return this.authService.signUp(action.username, action.password).pipe(map((data:any) => {
    //             this.store.dispatch(setLoadingSpinner({status: false}));
    //             this.store.dispatch(setErrorMessage({message: ''}));
    //             const user = this.authService.formatUser(data);
    //             this.authService.setUserInLocalStorage(user);
    //             return signupSuccess({user});
    //         }),
    //         catchError(error => {
    //             // console.log(error.error.error.message);
    //             this.store.dispatch(setLoadingSpinner({status: false}));
    //             const errorMessage = this.authService.getErrorMessage(error.error.error.message);
    //             return of(setErrorMessage({ message: errorMessage}));
    //         })
    //         );
    //     }))
    // });

    // signupRedirect$ = createEffect(() => {
    //     return this.actions$.pipe(ofType(signupSuccess), tap(action => {
    //         this.router.navigate(['/'])
    //     })
    //     )},{dispatch: false}
    // );
}