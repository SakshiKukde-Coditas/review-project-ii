import { loginSuccess, signupSuccess } from "./auth.actions";
import { initialState } from "./auth.state";
import { createReducer, on } from "@ngrx/store";

const _authReducer = createReducer(initialState, 
    on(loginSuccess, (state, action)=>{
        console.log(state);
        console.log(action);
    return { ...state, user: action.user}
}),
on(signupSuccess, (state, action) => {
    return { ...state, user: action.user }
})
);

export function AuthReducer(state:any, action:any ) {
    return _authReducer(state, action);
}