import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app.state';
import { signupStart } from '../state/auth.actions';
import { setLoadingSpinner } from 'src/app/state/shared/shared.action';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit{

  signUpForm!: FormGroup;

  constructor(private store: Store<AppState>){}

  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required,])
    })
  }
  onSignUp() {
    console.log("inside signup");
    if(!this.signUpForm.valid) {
      return;
    }
    const username = this.signUpForm.value.username;
    const password = this.signUpForm.value.password;
    this.store.dispatch(setLoadingSpinner({ status: true }));
    this.store.dispatch(signupStart({ username, password }));
  }
}
