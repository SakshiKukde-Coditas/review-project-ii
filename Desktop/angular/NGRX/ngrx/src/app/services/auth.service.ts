import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { User } from "../models/user.model";

@Injectable({
    providedIn: 'root'
})

export class AuthService {

    timeOutInterval: any;
    constructor(private http: HttpClient){}

    login(username: string, password:string) {
        console.log("inside login api call");
        console.log(username, password);
        return this.http.post<any>(`http://localhost:3000/login`,{username, password});
    }

    signUp(username: string, password: string) {
        console.log("inside signup api call");
        return this.http.post<any>(`http://localhost:3000/login`,{username, password});
    }

    formatUser(data: any) {
        const user = new User(data.username, data.token, data.message);
        return user;
    }

    getErrorMessage(message: string) {
        switch(message) {
            case 'USERNAME_NOT_FOUND':
                return 'Username not found';
                case 'INVALID_PASSWORD':
                    return 'Invalid Password';
                    default: 
                    return 'Unknown error occurred. Please ry again';
        }
    }


    //dont have signUp api so kept as commented

    // setUserInLocalStorage(user: User) {
    //     localStorage.setItem('userData', JSON.stringify(user));

    //     const todaysDate = new Date().getTime();
    //     const expirationDate = user?.expireDate?.getTime();
    //     const timeInterval = expirationDate - todaysDate;

    //     this.timeOutInterval = setTimeout(() => {
    //         //for logout functionality and getting refresh token
    //     }, timeInterval);
    // }
}