import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, map } from "rxjs";
import { Posts } from "../models/posts.model";
import { deletePost } from "../posts/state/posts.actions";

@Injectable({
    providedIn: 'root',
})

export class PostsService {

    constructor(private http: HttpClient){}

    getPosts(): Observable<Posts[]> {
        return this.http.get<Posts[]>(`http://localhost:3000/user/getAllUsers`);
    //     .pipe(map((data) => {
    //         const posts:Posts[] = [];
    //         for(let key in data) {
    //             posts.push({...data}); 
    //             // posts.push(...data[key]);
    //             console.log(data);
    //         }
    //         return posts;
    //     }))
    }

    addPosts(post: Posts) {
        return this.http.post(`http://localhost:3000/user/addUser`, post);
    } 

    updatePosts(post: Posts) {
        console.log("update posts",post);
        return this.http.put(`http://localhost:3000/user/updateUser`,post);
    }

    deletePosts(id: any) {
        console.log("delete post",id);
        return this.http.delete(`http://localhost:3000/user/deleteUser`,id);
    }
}