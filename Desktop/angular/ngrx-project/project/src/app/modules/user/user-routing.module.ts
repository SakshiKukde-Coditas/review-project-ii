import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './pages/users/users.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { ChangeDetectionComponent } from './pages/users/change-detection/change-detection.component';

// const routes: Routes = [
//   {
// 		path: '',
// 		component: UserDashboardComponent,
//     children: [
//       {
// 				path: 'users',
// 				component: UsersComponent,
// 			},
// 			{
// 				path: 'change-detection',
// 				component: ChangeDetectionComponent,
// 			}
//     ]
//   }
// ];

const routes: Routes = [
	{
		path: '',
		component: UserDashboardComponent,
		children: [
		  {
			path: 'users',
			component: UsersComponent
		  },
		  {
			path: 'change-detection',
			component: ChangeDetectionComponent
		  }
		]
	  }
  ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
