import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UsersComponent } from './pages/users/users.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';

import { AddUserDialogComponent } from './components/add-user-dialog/add-user-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/angular-material.modules';
import { ChangeDetectionComponent } from './pages/users/change-detection/change-detection.component';
import { DeleteUserDialogComponent } from './components/delete-user-dialog/delete-user-dialog.component';


@NgModule({
  declarations: [
    UsersComponent,
    UserDashboardComponent,
    AddUserDialogComponent,
    ChangeDetectionComponent,
    DeleteUserDialogComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule
  ]
})
export class UserModule { }
