import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserDataService } from 'src/app/core/services/user-data/user-data.service';
import { DeleteUserDialogComponent } from '../components/delete-user-dialog/delete-user-dialog.component';
import { AddUserDialogComponent } from '../components/add-user-dialog/add-user-dialog.component';
import { takeWhile } from 'rxjs';
import { MatTreeNodeOutlet } from '@angular/material/tree';


@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit, OnDestroy{

  constructor(private userDataService: UserDataService, private router: Router,private dialog: MatDialog){}

  user:any[]=[];
  loading = false;
  error = false;
  isAlive = true;
  data:any = {};

  ngOnInit(): void {
    this.getUsers();
  }
  ngOnDestroy(): void {
    this.isAlive = false;
  }

  getUsers() {
    const observer$ = this.userDataService.getAllUsers();
    const userData$ = observer$[1];
    const loading$ = observer$[0];
    const error$ = observer$[2];
    userData$.pipe(takeWhile(()=> this.isAlive)).subscribe(response =>{
      console.log(response);
      this.user = response;
    });
    loading$.pipe(takeWhile(()=> this.isAlive)).subscribe(response =>{
      this.loading = response;
    });
    error$.pipe(takeWhile(()=> this.isAlive)).subscribe(response => {
      this.error = response;
    })
  }

  deleteUser(id:number) {
    console.log(id);
    console.log("inside delete user");
    const dialogRef= this.dialog.open(DeleteUserDialogComponent,{
      width: '300px',
        data: 'Are you sure you want to delete?',
    })
    dialogRef.afterClosed().subscribe((res)=>{
      if(res) {
        console.log("res",res);
        const observer$ = this.userDataService.deleteUser(id);
        const userData$ = observer$[1];
        const loading$ = observer$[0];
        const error$ = observer$[2];
        userData$.pipe(takeWhile(()=> this.isAlive)).subscribe(response =>{
        console.log(response,"inside userData");
        this.user = response;
        this.getUsers();
    });
    loading$.pipe(takeWhile(()=> this.isAlive)).subscribe(response =>{
      this.loading = response;
    });
    error$.pipe(takeWhile(()=> this.isAlive)).subscribe(response => {
      this.error = response;
    })
    }
    });
  }

  updateUser(data:any) {
    console.log(data);
    if(data.operation === 'add') {
      const dialogRef= this.dialog.open(AddUserDialogComponent,{
        width: '400px',
        data: data,
      })
      dialogRef.afterClosed().subscribe((res)=>{
        if(res) {
          console.log("res",res);
          const observer$ = this.userDataService.addUser(res);
          console.log(observer$);
          const userData$ = observer$[1];
          const loading$ = observer$[0];
          const error$ = observer$[2];
          userData$.subscribe(response =>{
          console.log(response,"inside update data");
          this.user = response;
          this.getUsers();
      });
      loading$.subscribe(response =>{
        this.loading = response;
      });
      error$.subscribe(response => {
        this.error = response;
      })
      }
      });
    }
    else {
      const userData = Object.assign({operation: 'edit'}, data);
      const dialogRef= this.dialog.open(AddUserDialogComponent,{
        width: '400px',
        data: userData,
      })
      dialogRef.afterClosed().subscribe((res)=>{
        if(res) {
          console.log("res",res);
          const observer$ = this.userDataService.updateUser(res);
          console.log(observer$);
          const userData$ = observer$[1];
          const loading$ = observer$[0];
          const error$ = observer$[2];
          userData$.subscribe(response =>{
          console.log(response,"inside userData");
          this.user = response;
      });
      loading$.subscribe(response =>{
        this.loading = response;
      });
      error$.subscribe(response => {
        this.error = response;
      })
      }
      });
    }
   
  }

  // add(data:any) {
  //   console.log(data);
  //   // const userData = {
  //   //   operation: data
  //   // }
  //   this.dialog.open(AddUserDialogComponent,{
  //     width:'400px',
  //     data: userData
  //   });
  // }

  tryAgain() {
    console.log("inside try again");
    this.userDataService.getAllUsers(true);
  }

  tabChangeEventHandler(tabChangeEvent: any) {
    if (tabChangeEvent?.index === 0) {
      this.router.navigate(['users']);
      this.getUsers();
		} else if (tabChangeEvent?.index === 1) {
      this.router.navigate(['change-detection']);
		}
  }

}